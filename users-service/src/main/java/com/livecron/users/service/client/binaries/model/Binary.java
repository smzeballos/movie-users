package com.livecron.users.service.client.binaries.model;

import com.livecron.binaries.api.output.IBinary;

/**
 * @author Santiago Mamani
 */
public class Binary implements IBinary {

    private String id;

    private String mimeType;

    private String name;

    private Long size;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getSize() {
        return size;
    }
}

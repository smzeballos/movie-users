package com.livecron.users.service.controller;

// CTRL + lat + O

import com.livecron.users.api.input.AccountCreateInput;
import com.livecron.users.api.model.AccountState;
import com.livecron.users.service.bean.Asus;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.repository.AccountRepository;
import com.livecron.users.service.service.AccountAvatarUpdateService;
import com.livecron.users.service.service.AccountCreateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Santiago Mamani
 */

@RequestMapping(value = "/accounts")
@RequestScope
@RestController
public class AccountController {

    // @Autowired DI por propiedad
    @Autowired
    private AccountCreateService accountCreateService;

    @Autowired
    private AccountAvatarUpdateService accountAvatarUpdateService;

    @Autowired
    private Asus asus;

    @Autowired
    private AccountRepository accountRepository;

    @ApiOperation(
            value = "This endpoint is to create account"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "Account created"),
            @ApiResponse(code = 403, message = "Acceso denegado")
    })
    @RequestMapping(method = RequestMethod.POST)
    public Account createAccount(@RequestBody AccountCreateInput input) {
        accountCreateService.setInput(input);
        accountCreateService.execute();

        return accountCreateService.getAccount();
    }

    @RequestMapping(
            value = "/data",
            method = RequestMethod.GET
    )
    public Account findAccountByEmailAndState(@RequestParam("email") String email,
                                              @RequestParam("state") AccountState state) {
        return accountRepository.findByEmailAndState(email, state).orElse(null);
    }

    @RequestMapping(
            value = "/bean",
            method = RequestMethod.GET
    )
    public Asus findAsus() {
        System.out.println("Price: " + asus.getPrice());//Ctrl +D
        System.out.println("Description: " + asus.getDescription());
        asus.setPrice(12000);
        return asus;
    }

    @RequestMapping(
            value = "/beanSecond",
            method = RequestMethod.GET
    )
    public Asus findAsusSecond() {
        System.out.println("Price: " + asus.getPrice());//Ctrl +D
        System.out.println("Description: " + asus.getDescription());
        return asus;
    }

    @RequestMapping(
            value = "/{accountId}/avatar",
            method = RequestMethod.PUT,
            consumes = "multipart/form-data"
    )
    public Account updateAvatar(@PathVariable("accountId") Long accountId,
                                @RequestPart("multipartFile") MultipartFile multipartFile) {
        accountAvatarUpdateService.setAccountId(accountId);
        accountAvatarUpdateService.setFile(multipartFile);
        accountAvatarUpdateService.execute();

        return accountAvatarUpdateService.getAccount();
    }
}

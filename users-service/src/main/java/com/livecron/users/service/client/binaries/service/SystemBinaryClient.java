package com.livecron.users.service.client.binaries.service;

import com.livecron.users.service.client.binaries.model.Binary;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Santiago Mamani
 */
@FeignClient(name = "binaries-service")
interface SystemBinaryClient {

    @RequestMapping(
            value = "/system/binaries",
            method = RequestMethod.POST,
            consumes = "multipart/form-data"
    )
    Binary uploadBinary(@PathVariable(value = "multipartFile") MultipartFile multipartFile);
}
